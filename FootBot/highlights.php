<?php
$teams = '{
    "Russia":"🇷🇺",
    "Saudi Arabia":"🇸🇦",
    "Spain":"🇪🇸",
    "Portugal":"🇵🇹",
    "England":"🏴󠁧󠁢󠁥󠁮󠁧󠁿",
    "Egypt":"🇪🇬",
    "Senegal":"🇸🇳",
    "Germany":"🇩🇪",
    "Croatia":"🇭🇷",
    "Argentina":"🇦🇷",
    "Brazil":"🇧🇷",
    "Uruguay":"🇺🇾",
    "France":"🇫🇷",
    "Colombia":"🇨🇴",
    "Iran":"🇮🇷",
    "South Korea":"🇰🇷",
    "Australia":"🇦🇺",
    "Nigeria":"🇳🇬",
    "Poland":"🇵🇱",
    "Iceland":"🇮🇸",
    "Serbia":"🇷🇸",
    "Panama":"🇵🇦",
    "Swistzerland":"🇨🇭",
    "Morocco":"🇲🇦",
    "Tunisia":"🇹🇳",
    "Denmark":"🇩🇰",
    "Costa Rica":"🇨🇷",
    "Japan":"🇯🇵",
    "Mexico":"🇲🇽",
    "Peru":"🇵🇪",
    "Sweden":"🇸🇪",
    "Belgium":"🇧🇪"
}';

$teamFlags = json_decode($teams,true);
$countryID = 340;
$to = date('Y-m-d');
$from = date('Y-m-d',(strtotime ( '-5 day' , strtotime ($to) ) ));
$date = date('YY-MM-DD');

if($request["result"]["parameters"]["team1"] && $request["result"]["parameters"]["team2"])
{
  $team1 = $request["result"]["parameters"]["team1"];
  $team2 = $request["result"]["parameters"]["team2"];
  if($team1 == "Russian Federation")
  {
  $team1 = "Russia";
  }
  
  if($team2 == "Russian Federation")
  {
  $team2 = "Russia";
  }

  if($team1 == "Islamic Republic of Iran")
  {
  $team1 = "Iran";
  }
  
  if($team2 == "Islamic Republic of Iran")
  {
  $team2 = "Iran";
  }

  

  $group = $team1." vs ".$team2;
}

$homepenalty = 0;
$awaypenalty = 0;
$groupMatches = "⚽ ".$group." Result ⚽\n------------------------------ \n";
$myanswer = "";

            $curl_options = array(
                        CURLOPT_URL => "https://apifootball.com/api/?action=get_events&from=$from&to=$to&country_id=340&APIkey=$APIkey",
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_HEADER => false,
                        CURLOPT_TIMEOUT => 30,
                        CURLOPT_CONNECTTIMEOUT => 0
                          );

             $curl = curl_init();
             curl_setopt_array( $curl, $curl_options );
             $result1 = curl_exec( $curl );

             $result = (array) json_decode($result1);
             foreach( $result as $element)
             {
                if($element -> match_status == "FT"|| strtotime($element -> match_date) < strtotime($date) || $element -> match_status == "AET")
            {
         
               if(($element -> match_hometeam_name == $team1 ||$element -> match_awayteam_name == $team1) && ($element -> match_hometeam_name == $team2 ||$element -> match_awayteam_name == $team2))
              {
                  foreach($teamFlags as $key => $value)
                       {
                         if($element -> match_hometeam_name == $key )
                                 $homeflag = $value;
                        elseif( $element -> match_awayteam_name == $key)
                                $awayflag = $value;
                       }

                  
                  $results = $homeflag." *".$element -> match_hometeam_name."* (".$element-> match_hometeam_score.")  vs ".$awayflag." *".$element -> match_awayteam_name."* (".$element->match_awayteam_score.") \n  ";
                   if($element -> match_status == "AET")
                    $results .= "------------------------------\n*Penalties*\n".$homeflag." *".$element -> match_hometeam_name."* (".$element-> match_hometeam_penalty_score.")  vs ".$awayflag." *".$element -> match_awayteam_name."* (".$element->match_awayteam_penalty_score.")\n------------------------------\n";
                           
        foreach($element -> goalscorer as $goalscored)
         {
          if($goalscored -> home_scorer && $goalscored -> away_scorer)
            {
              if( $homepenalty == (int) substr($goalscored -> score,0,1))
                {
                  $homepenalty = (int) substr($goalscored -> score,0,1);
                  $homegoalIcon = "❌";
                  }
               else {
                  $homepenalty = (int) substr($goalscored -> score,0,1);
                  $homegoalIcon = "⚽";
                  }
              if($awaypenalty == (int) substr($goalscored -> score,4,1)){
                  $awaypenalty = (int) substr($goalscored -> score,4,1);
                  $awaygoalIcon = "❌";
                 }
               else {
                  $awaypenalty = (int) substr($goalscored -> score,4,1);
                  $awaygoalIcon = "⚽";
                  }

                $homegoalScorer = $goalscored -> home_scorer;
               foreach($teamFlags as $key => $value)
                 {
              if($element -> match_hometeam_name == $key)
                  $homeflag = $value;
                }
                foreach($teamFlags as $key => $value)
                 {
              if($element -> match_awayteam_name == $key)
                  $awayflag = $value;
                }
               $awaygoalScorer = $goalscored -> away_scorer;
               
              $results .= $homegoalIcon." ".$homeflag." ".$homegoalScorer."(120') \n".$awaygoalIcon." ".$awayflag." ".$awaygoalScorer."(120') \n ";
               
          }
          else
            {
              if($goalscored -> home_scorer)
             {
               $goalScorer = $goalscored -> home_scorer;
               foreach($teamFlags as $key => $value)
                 {
              if($element -> match_hometeam_name == $key)
                  $flag = $value;
                }
             }
             elseif ($goalscored -> away_scorer)
             {
               foreach($teamFlags as $key => $value)
                 {
              if($element -> match_awayteam_name == $key)
                  $flag = $value;
                }
               $goalScorer = $goalscored -> away_scorer;
               }

             $goalIcon = "⚽";
             $time =   $goalscored -> time;
             if($time == "&nbsp;")
             {
              $time = "120'";
              $goalIcon = "❌";
             }
              $results .= $goalIcon." ".$flag." ".$goalScorer."(".$time.") \n ";
            }
         }
         foreach($element -> cards as $card)
         {
             if($card -> home_fault)
             {
               $cardHolder = $card -> home_fault;
               foreach($teamFlags as $key => $value)
                 {
              if($element -> match_hometeam_name == $key)
                  $flag = $value;
                }
             }
             elseif ($card -> away_fault)
             {
               foreach($teamFlags as $key => $value)
                 {
              if($element -> match_awayteam_name == $key)
                  $flag = $value;
                }
               $cardHolder = $card -> away_fault;
               }
            if($card -> card == 'yellowcard')
             {
               $cardcolor = "⚠️";
             }
            elseif($card -> card == 'redcard')
             {
               $cardcolor = "🔴";
             }
         $results .= $cardcolor." ".$flag." ".$cardHolder."(".$card -> time.") \n ";
         }
         $results .= "------------------------------ ";

                   $curl_options1 = array(
                        CURLOPT_URL => "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=50&playlistId=PLCGIzmTE4d0hww7NG9ytmooEUZov2k-23&key=AIzaSyBoDYURmFEAuMNEVqxEbpas_Kwmd4j69f4",
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_HEADER => false,
                        CURLOPT_TIMEOUT => 30,
                        CURLOPT_CONNECTTIMEOUT => 5
                        );

                  $curl1 = curl_init();
                  curl_setopt_array( $curl1, $curl_options1 );
                  $resultYoutube = curl_exec( $curl1 );

                  $youtubeResult = (array) json_decode($resultYoutube);
                  foreach($youtubeResult['items'] as $element) {
                    if(is_numeric(strpos($element -> snippet -> title, $team1)) && is_numeric(strpos($element -> snippet -> title, $team2)))
                    {
                      $youtubeLink = "https://www.youtube.com/watch?v=".$element -> snippet -> resourceId -> videoId;
                    }           
                  }

                  $myanswer = $results." \n *Watch Highlights* \n".$youtubeLink."\n";

              }
            }
      }

  $endadd = "------------------------------ \nTo get statistics of each match send *Stats Team1 Team2* \n eg Send *Stats Portugal Spain* for stats of Portugal Vs Spain game";
$finalanswer = $groupMatches. $myanswer.$endadd;

 $output["speech"] = '"'.$finalanswer.'"'; 
 $output["displayText"] = '"'.$finalanswer.'"'; 
 $output["source"] = "whatever.php";
//  ob_end_clean(); 
 echo json_encode($output); 

?>