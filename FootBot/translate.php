<?php

/* Written by Amit Agarwal */
/* web: ctrlq.org          */


  $url = "https://translate.googleapis.com/translate_a/single?client=gtx&sl=auto&tl=en&dt=t&q=hola";
  
$curl_options = array(
  CURLOPT_URL => $url,
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_HEADER => false,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_CONNECTTIMEOUT => 0
);

$curl = curl_init();
curl_setopt_array( $curl, $curl_options );
$result1 = curl_exec( $curl );

$result = (array) json_decode($result1);

print_r($result[8][0][0]);
?>