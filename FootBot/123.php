<?php 

// This is your webhook. You must configure it in the number settings section.

$data1 = '{ 
 "id": "7797", 
 "number": "[YOUR NUMBER]", 
 "from": "[FROM NUMBER]", 
 "to": "[TO NUMBER]", 
 "type": "IN", 
 "text": "puntuación en vivo",	
 "creation_date": "2017-03-18 14:49:23" 
}';

// $data = json_decode($_POST["data"]); 

$data = json_decode($data1); 


$sessionID = uniqid('',true);
$lang;
try {
    // create curl resource
    $ch = curl_init();
    // set url
    curl_setopt($ch, CURLOPT_URL, "https://api.api.ai/v1/query?v=20150910&e=WELCOME&sessionId=" . $sessionID);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: Bearer 62b20eba8fa346cd8062b7189c27c004'));
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    //return the transfer as a string
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    // $output contains the output string
    $output = curl_exec($ch);
    // close curl resource to free up system resources
    curl_close($ch);
}catch (Exception $e) {
    $speech = $e->getMessage();
    $fulfillment = new stdClass();
    $fulfillment->speech = $speech;
    $result = new stdClass();
    $result->fulfillment = $fulfillment;
    $response = new stdClass();
    $response->result = $result;
    echo json_encode($response);
}

try {
    // if(isset($_POST['submit'])){
        $query = $data->text;

        $url = "https://translate.googleapis.com/translate_a/single?client=gtx&sl=auto&tl=en&dt=t&q=".urlencode($query);
  
        $curl_options = array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_HEADER => false,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_CONNECTTIMEOUT => 0
        );

        $curl = curl_init();
        curl_setopt_array( $curl, $curl_options );
        $resultTranslate = curl_exec( $curl );

        $TranslatedResult = (array) json_decode($resultTranslate);

        $translatedText = $TranslatedResult[0][0][0];
        $lang = $TranslatedResult[8][0][0];
        $postData = array('query' => array($translatedText), 'lang' => 'en', 'sessionId' => $sessionID);
        $jsonData = json_encode($postData);
        $v = date('Ymd');
        $ch = curl_init('https://api.api.ai/v1/query?v='.$v);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: Bearer 62b20eba8fa346cd8062b7189c27c004'));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $dialogflowresult = json_decode($result,true);
        curl_close($ch);
    // }
}
catch (Exception $e) {
    $speech = $e->getMessage();
    $fulfillment = new stdClass();
    $fulfillment->speech = $speech;
    $result = new stdClass();
    $result->fulfillment = $fulfillment;
    $response = new stdClass();
    $response->result = $result;
    echo json_encode($response);
}



// // When you receive a message an INBOX event is created 
// if ($data->event=="INBOX") 
// { 
  $my_answer = $dialogflowresult['result']['fulfillment']['speech'];
  if(substr($my_answer, 0 , 1) == '"')
  {
   $my_answer = substr($my_answer,1,-1);
  }
//   else{
      $url = "https://translate.googleapis.com/translate_a/single?client=gtx&sl=auto&tl=".$lang."&dt=t&q=".urlencode($my_answer);
  
        $curl_options = array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_HEADER => false,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_CONNECTTIMEOUT => 0
        );

        $curl = curl_init();
        curl_setopt_array( $curl, $curl_options );
        $resultTranslate = curl_exec( $curl );

        $TranslatedResult = (array) json_decode($resultTranslate);

        $my_answer = $TranslatedResult[0][0][0];
//   }
   


  $response = new StdClass(); 
  $response->apiwha_autoreply = $my_answer; // Attribute "apiwha_autoreply" is very important! 

  echo json_encode($response); // Don't forget to reply in JSON format 

  /* You don't need any APIKEY to answer to your customer from a webhook */ 

// } 

?>