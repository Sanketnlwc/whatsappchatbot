<?php
$flag;
$teams = '{
    "Russia":"🇷🇺",
    "Saudi Arabia":"🇸🇦",
    "Spain":"🇪🇸",
    "Portugal":"🇵🇹",
    "England":"🏴󠁧󠁢󠁥󠁮󠁧󠁿",
    "Egypt":"🇪🇬",
    "Senegal":"🇸🇳",
    "Germany":"🇩🇪",
    "Croatia":"🇭🇷",
    "Argentina":"🇦🇷",
    "Brazil":"🇧🇷",
    "Uruguay":"🇺🇾",
    "France":"🇫🇷",
    "Colombia":"🇨🇴",
    "Iran":"🇮🇷",
    "South Korea":"🇰🇷",
    "Australia":"🇦🇺",
    "Nigeria":"🇳🇬",
    "Poland":"🇵🇱",
    "Iceland":"🇮🇸",
    "Serbia":"🇷🇸",
    "Panama":"🇵🇦",
    "Swistzerland":"🇨🇭",
    "Morocco":"🇲🇦",
    "Tunisia":"🇹🇳",
    "Denmark":"🇩🇰",
    "Costa Rica":"🇨🇷",
    "Japan":"🇯🇵",
    "Mexico":"🇲🇽",
    "Peru":"🇵🇪",
    "Sweden":"🇸🇪",
    "Belgium":"🇧🇪"
}';

$teamFlags = json_decode($teams,true);

$leagueIdJson = '{
    "GroupA":"1736",
    "GroupB":"1737",
    "GroupC":"1738",
    "GroupD":"1739",
    "GroupE":"1740",
    "GroupF":"1741",
    "GroupG":"1742",
    "GroupH":"1743"
}';

$leagueIdArr = json_decode($leagueIdJson,true);

$group = $request["result"]["parameters"]["parameter"];
if($request["result"]["parameters"]["split"])
{
  $group = $request["result"]["parameters"]["split"][0].$request["result"]["parameters"]["split"][1];
}
// $group = "GroupA";

$group = ucfirst($group);
foreach($leagueIdArr as $key => $value){
    if($key == $group)
      $leagueId = $value;
}


$curl_options = array(
  CURLOPT_URL => "https://apifootball.com/api/?action=get_standings&league_id=$leagueId&APIkey=$APIkey",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_HEADER => false,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_CONNECTTIMEOUT => 5
);

$displayGroup = str_split($group,5);

$curl = curl_init();
curl_setopt_array( $curl, $curl_options );
$result1 = curl_exec( $curl );
$ongoing = " *Standings ".$displayGroup[0]." ".$displayGroup[1]."* \n \n";
$myanswer = "";


$result = (array) json_decode($result1);


function sort_objects_by_total($a, $b) {

	if($a->overall_league_position == $b->overall_league_position){ return 0 ; }
	return ($a->overall_league_position < $b->overall_league_position) ? -1 : 1;
}

usort($result, 'sort_objects_by_total');


 foreach( $result as $element){
    foreach($teamFlags as $key => $value)
    {
        if($element -> team_name == $key)
            $flag = $value;
    }
    $myanswer .= $flag." *".$element -> team_name." - ".$element -> overall_league_PTS." points* \n (".$element-> overall_league_W."W, ".$element -> overall_league_L."L, ".$element -> overall_league_D."D) \n";
} 

$finalanswer = $ongoing.$myanswer;

 $output["speech"] = '"'.$finalanswer.'"'; 
 $output["displayText"] = '"'.$finalanswer.'"'; 
 $output["source"] = "whatever.php";
 ob_end_clean();
 echo json_encode($output); 


?>