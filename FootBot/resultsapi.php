<?php

$flag;
$teams = '{
    "Russia":"🇷🇺",
    "Saudi Arabia":"🇸🇦",
    "Spain":"🇪🇸",
    "Portugal":"🇵🇹",
    "England":"🏴󠁧󠁢󠁥󠁮󠁧󠁿",
    "Egypt":"🇪🇬",
    "Senegal":"🇸🇳",
    "Germany":"🇩🇪",
    "Croatia":"🇭🇷",
    "Argentina":"🇦🇷",
    "Brazil":"🇧🇷",
    "Uruguay":"🇺🇾",
    "France":"🇫🇷",
    "Colombia":"🇨🇴",
    "Iran":"🇮🇷",
    "South Korea":"🇰🇷",
    "Australia":"🇦🇺",
    "Nigeria":"🇳🇬",
    "Poland":"🇵🇱",
    "Iceland":"🇮🇸",
    "Serbia":"🇷🇸",
    "Panama":"🇵🇦",
    "Swistzerland":"🇨🇭",
    "Morocco":"🇲🇦",
    "Tunisia":"🇹🇳",
    "Denmark":"🇩🇰",
    "Costa Rica":"🇨🇷",
    "Japan":"🇯🇵",
    "Mexico":"🇲🇽",
    "Peru":"🇵🇪",
    "Sweden":"🇸🇪",
    "Belgium":"🇧🇪"
}';

$teamFlags = json_decode($teams,true);
$countryID = 340;

$leagueIdJson = '{
    "GroupA":"1736",
    "GroupB":"1737",
    "GroupC":"1738",
    "GroupD":"1739",
    "GroupE":"1740",
    "GroupF":"1741",
    "GroupG":"1742",
    "GroupH":"1743"
}';
$leagueIdArr = json_decode($leagueIdJson,true);
$date = date('YY-MM-DD');
$from = '2018-06-14';
$to = '2018-07-15';
$leagueId;
if($request["result"]["parameters"]["parameter"])
{
$group = $request["result"]["parameters"]["parameter"];
}
if($request["result"]["parameters"]["country"])
{
$group = $request["result"]["parameters"]["country"];
}
if($request["result"]["parameters"]["split"])
{
  $group = $request["result"]["parameters"]["split"][0].$request["result"]["parameters"]["split"][1];
}

if($group == "Russian Federation")
{
  $group = "Russia";
}


if($group == "Islamic Republic Of Iran")
{
  $group = "Iran";
}
// $group = 'GroupA';
if($group)
$group = ucfirst($group);

$groupMatches = "⚽ ".$group." Results ⚽\n";
$myanswer = "";

 if($request["result"]["parameters"]["parameter"] || $request["result"]["parameters"]["split"])
 {
    foreach($leagueIdArr as $key => $value){
       if($key == $group)
         $leagueId = $value;
            }

$curl_options = array(
  CURLOPT_URL => "https://apifootball.com/api/?action=get_events&from=$from&to=$to&league_id=$leagueId&APIkey=$APIkey",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_HEADER => false,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_CONNECTTIMEOUT => 0
);

$curl = curl_init();
curl_setopt_array( $curl, $curl_options );
$result1 = curl_exec( $curl );

$result = (array) json_decode($result1);

//   function sort_objects_by_total($a, $b) {
// return strcmp(strtotime($a -> match_date." ".$a -> match_time), strtotime($b -> match_date." ".$b -> match_time));
// }


// usort($result, 'sort_objects_by_total');

       foreach($result as $element) {
        if($element -> match_status == "FT" || strtotime($element -> match_date) < strtotime($date) || $element -> match_status == "AET")
            {
                foreach($teamFlags as $key => $value)
                 {
              if($element -> match_hometeam_name == $key )
                  $homeflag = $value;
                elseif( $element -> match_awayteam_name == $key)
                  $awayflag = $value;
                }
         $myanswer .= $homeflag." *".$element ->match_hometeam_name."* (".$element->match_hometeam_score.")  vs ".$awayflag." *".$element -> match_awayteam_name."* (".$element->match_awayteam_score.") \n  ";
         if($element -> match_status == "AET")
                    $myanswer .= "------------------------------\n*Penalties*\n".$homeflag." *".$element -> match_hometeam_name."* (".$element-> match_hometeam_penalty_score.")  vs ".$awayflag." *".$element -> match_awayteam_name."* (".$element->match_awayteam_penalty_score.")\n------------------------------\n";
        }
    }
}
elseif($request["result"]["parameters"]["country"])
{

$curl_options = array(
  CURLOPT_URL => "https://apifootball.com/api/?action=get_events&from=$from&to=$to&country_id=$countryID&APIkey=$APIkey",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_HEADER => false,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_CONNECTTIMEOUT => 0
);

$curl = curl_init();
curl_setopt_array( $curl, $curl_options );
$result1 = curl_exec( $curl );

$result = (array) json_decode($result1);

 function sort_objects_by_total($a, $b) {
return strcmp(strtotime($b -> match_date." ".$b -> match_time), strtotime($a -> match_date." ".$a -> match_time));
}


usort($result, 'sort_objects_by_total');
foreach($result as $element) {
        if($element -> match_status == "FT"|| strtotime($element -> match_date) < strtotime($date) ||  $element -> match_status == "AET")
            {
                if($element -> match_hometeam_name == $group || $element -> match_awayteam_name == $group)
                 {
                       foreach($teamFlags as $key => $value)
                       {
                         if($element -> match_hometeam_name == $key )
                                 $homeflag = $value;
                        elseif( $element -> match_awayteam_name == $key)
                          $awayflag = $value;
                       }
                  $myanswer .= $homeflag." *".$element -> match_hometeam_name."* (".$element-> match_hometeam_score.")  vs ".$awayflag." *".$element -> match_awayteam_name."* (".$element->match_awayteam_score.") \n  ";
                 if($element -> match_status == "AET")
                    $myanswer .= "------------------------------\n*Penalties*\n".$homeflag." *".$element -> match_hometeam_name."* (".$element-> match_hometeam_penalty_score.")  vs ".$awayflag." *".$element -> match_awayteam_name."* (".$element->match_awayteam_penalty_score.")\n------------------------------\n";
                  }   
            }
        }
}
else {
  {

$curl_options = array(
  CURLOPT_URL => "https://apifootball.com/api/?action=get_events&from=$from&to=$to&country_id=$countryID&APIkey=$APIkey",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_HEADER => false,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_CONNECTTIMEOUT => 0
);

$curl = curl_init();
curl_setopt_array( $curl, $curl_options );
$result1 = curl_exec( $curl );

$result = (array) json_decode($result1);


  function sort_objects_by_total($a, $b) {
return strcmp(strtotime($b -> match_date." ".$b -> match_time), strtotime($a -> match_date." ".$a -> match_time));
}


usort($result, 'sort_objects_by_total');
foreach($result as $element) {
        if($element -> match_status == "FT" || strtotime($element -> match_date) < strtotime($date) || $element -> match_status == "AET")
            {
                       foreach($teamFlags as $key => $value)
                       {
                         if($element -> match_hometeam_name == $key )
                                 $homeflag = $value;
                        elseif( $element -> match_awayteam_name == $key)
                          $awayflag = $value;
                       }
                  $myanswer .= $homeflag." *".$element -> match_hometeam_name."* (".$element-> match_hometeam_score.")  vs ".$awayflag." *".$element -> match_awayteam_name."* (".$element->match_awayteam_score.")\n";
                  if($element -> match_status == "AET")
                    $myanswer .= "------------------------------\n*Penalties*\n".$homeflag." *".$element -> match_hometeam_name."* (".$element-> match_hometeam_penalty_score.")  vs ".$awayflag." *".$element -> match_awayteam_name."* (".$element->match_awayteam_penalty_score.")\n------------------------------\n";
                  
                  
                 
            }
        }
}
}

$endadd = "------------------------------ \nTo get statistics of each match send *Stats Team1 Team2* \n eg Send *Stats Portugal Spain* for stats of Portugal Vs Spain game";
$finalanswer = $groupMatches. $myanswer.$endadd;

 $output["speech"] = '"'.$finalanswer.'"'; 
 $output["displayText"] = '"'.$finalanswer.'"'; 
 $output["source"] = "whatever.php";
//  ob_end_clean(); 
 echo json_encode($output); 
?>
