<?php
// $group = 'GroupA';
date_default_timezone_set("Asia/Kolkata");
$date = date('d F Y');
$time = date('h:i a');
$to = date('Y-m-d');
$from = date('Y-m-d',(strtotime ( '-1 day' , strtotime ($to) ) ));
$countryID = 340;

$teams = '{
    "Russia":"🇷🇺",
    "Saudi Arabia":"🇸🇦",
    "Spain":"🇪🇸",
    "Portugal":"🇵🇹",
    "England":"🏴󠁧󠁢󠁥󠁮󠁧󠁿",
    "Egypt":"🇪🇬",
    "Senegal":"🇸🇳",
    "Germany":"🇩🇪",
    "Croatia":"🇭🇷",
    "Argentina":"🇦🇷",
    "Brazil":"🇧🇷",
    "Uruguay":"🇺🇾",
    "France":"🇫🇷",
    "Colombia":"🇨🇴",
    "Iran":"🇮🇷",
    "South Korea":"🇰🇷",
    "Australia":"🇦🇺",
    "Nigeria":"🇳🇬",
    "Poland":"🇵🇱",
    "Iceland":"🇮🇸",
    "Serbia":"🇷🇸",
    "Panama":"🇵🇦",
    "Swistzerland":"🇨🇭",
    "Morocco":"🇲🇦",
    "Tunisia":"🇹🇳",
    "Denmark":"🇩🇰",
    "Costa Rica":"🇨🇷",
    "Japan":"🇯🇵",
    "Mexico":"🇲🇽",
    "Peru":"🇵🇪",
    "Sweden":"🇸🇪",
    "Belgium":"🇧🇪"
}';

$teamFlags = json_decode($teams,true);


$curl_options = array(
  CURLOPT_URL => "http://demo3272298.mockable.io/fixtures",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_HEADER => false,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_CONNECTTIMEOUT => 5
);

$curl = curl_init();
curl_setopt_array( $curl, $curl_options );
$result1 = curl_exec( $curl );
$myanswer = "";

$result = (array) json_decode($result1);

function sort_objects_by_total($a, $b) {
return strcmp(strtotime($a -> date." ".$a -> time), strtotime($b -> date." ".$b -> time));
}


usort($result["Worldcup"], 'sort_objects_by_total');

// function sort_objects_by_time($a, $b) {

// 	if($a->date == $b->date){ return 0 ; }
// 	return ($a->time < $b->time) ? -1 : 1;
// }

// usort($result["Worldcup"], 'sort_objects_by_time');

       foreach($result["Worldcup"] as $element1) {
           
            
if(strtotime($date." ".$time) <= strtotime($element1 -> date." ".$element1 -> time))
// if(strtotime("2018-06-15 06:58 pm") < strtotime("2018-06-15 8:30 pm"))
        {
            $curl_options = array(
  CURLOPT_URL => "https://apifootball.com/api/?action=get_events&from=$from&to=$to&country_id=340&APIkey=$APIkey",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_HEADER => false,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_CONNECTTIMEOUT => 0
);

$curl = curl_init();
curl_setopt_array( $curl, $curl_options );
$result1 = curl_exec( $curl );
$myanswer = "";

$result = (array) json_decode($result1);

foreach( $result as $element){
    if((strpos($element1 -> team1 , $element -> match_hometeam_name ) ||strpos($element1 -> team1 , $element -> match_awayteam_name )) && (strpos($element1 -> team2 , $element -> match_hometeam_name ) ||strpos($element1 -> team2 , $element -> match_awayteam_name )))
    //  print_r($element);{}
    {
               foreach($teamFlags as $key => $value)
                 {
              if($element -> match_hometeam_name == $key)
                  $homeflag = $value;
              if($element -> match_awayteam_name == $key)
                  $awayflag = $value;
                }
            //   foreach(  as $lineup){
                $myanswer .= "*".$element -> match_hometeam_name."* Lineup \n *Formation :* " .$element -> match_hometeam_system."\n";
                  foreach($element -> lineup -> home -> starting_lineups as $homelineup)
                  {
                      $myanswer .= $homeflag." ".$homelineup -> lineup_number." ".$homelineup -> lineup_player."\n";
                  }

                  function sort_objects_by_awaypos($a, $b) {
                                    return $a-> lineup_position > $b -> lineup_position;
                            }

                  usort($element -> lineup -> away -> starting_lineups, 'sort_objects_by_awaypos');


                  $myanswer .= "------------------------------ \n *".$element -> match_awayteam_name."* Lineup \n *Formation :* " .$element -> match_awayteam_system."\n";
                  foreach($element -> lineup -> away -> starting_lineups as $awaylineup)
                  {
                      $myanswer .= $awayflag." ".$awaylineup -> lineup_number." ".$awaylineup -> lineup_player."\n";
                  }
            //   }
           }
    }
           break;
    }
}
// $finalanswer = $groupMatches. $myanswer;

$endadd = "------------------------------ \n _This is a Predicted lineup before match starts_";
 $finalanswer = $myanswer.$endadd;
 $output["speech"] = '"'.$finalanswer.'"'; 
 $output["displayText"] = '"'.$finalanswer.'"'; 
 $output["source"] = "whatever.php";
//  ob_end_clean(); 
 echo json_encode($output ); 
?>