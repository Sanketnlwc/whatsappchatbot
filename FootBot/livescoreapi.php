<?php

date_default_timezone_set("Asia/Kolkata");
$to = date('Y-m-d');
$from = date('Y-m-d',(strtotime ( '-1 day' , strtotime ($to) ) ));
$countryID = 340;
$teams = '{
    "Russia":"🇷🇺",
    "Saudi Arabia":"🇸🇦",
    "Spain":"🇪🇸",
    "Portugal":"🇵🇹",
    "England":"🏴󠁧󠁢󠁥󠁮󠁧󠁿",
    "Egypt":"🇪🇬",
    "Senegal":"🇸🇳",
    "Germany":"🇩🇪",
    "Croatia":"🇭🇷",
    "Argentina":"🇦🇷",
    "Brazil":"🇧🇷",
    "Uruguay":"🇺🇾",
    "France":"🇫🇷",
    "Colombia":"🇨🇴",
    "Iran":"🇮🇷",
    "South Korea":"🇰🇷",
    "Australia":"🇦🇺",
    "Nigeria":"🇳🇬",
    "Poland":"🇵🇱",
    "Iceland":"🇮🇸",
    "Serbia":"🇷🇸",
    "Panama":"🇵🇦",
    "Swistzerland":"🇨🇭",
    "Morocco":"🇲🇦",
    "Tunisia":"🇹🇳",
    "Denmark":"🇩🇰",
    "Costa Rica":"🇨🇷",
    "Japan":"🇯🇵",
    "Mexico":"🇲🇽",
    "Peru":"🇵🇪",
    "Sweden":"🇸🇪",
    "Belgium":"🇧🇪"
}';

$teamFlags = json_decode($teams,true);
// if($time > "00:00:00" && $time < "03:00:00")
//   {
//   $from = date('Y-m-d',(strtotime ( '-1 day' , strtotime ( $from) ) ));
//   $to = date('Y-m-d',(strtotime ( '-1 day' , strtotime ( $to) ) ));
//   }


$curl_options = array(
  CURLOPT_URL => "https://apifootball.com/api/?action=get_events&from=$from&to=$to&country_id=340&APIkey=$APIkey",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_HEADER => false,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_CONNECTTIMEOUT => 0
);

$curl = curl_init();
curl_setopt_array( $curl, $curl_options );
$result1 = curl_exec( $curl );
$ongoing = " *Ongoing Matches* \n ------------------------------ \n";
$myanswer = "";

$homepenalty = 0;
$awaypenalty = 0;

$result = (array) json_decode($result1);

foreach( $result as $element){
    if($element -> match_live == "1" || $element -> match_status == 'Pen')
     {
         $myanswer .= "*".$element ->match_hometeam_name."* (".$element->match_hometeam_score.")  vs *".$element -> match_awayteam_name."* (".$element->match_awayteam_score.") (".$element -> match_status.") \n  ";
         if($element -> match_status == 'Pen')
            $myanswer .= "*Penalties* /n *".$element ->match_hometeam_name."* (".$element->match_hometeam_penalty_score.")  vs *".$element -> match_awayteam_name."* (".$element->match_awayteam_penalty_score.") \n ";
         foreach($element -> goalscorer as $goalscored)
         {
          if($goalscored -> home_scorer && $goalscored -> away_scorer)
            {
              if( $homepenalty == (int) substr($goalscored -> score,0,1))
                {
                  $homepenalty = (int) substr($goalscored -> score,0,1);
                  $homegoalIcon = "❌";
                  }
               else {
                  $homepenalty = (int) substr($goalscored -> score,0,1);
                  $homegoalIcon = "⚽";
                  }
              if($awaypenalty == (int) substr($goalscored -> score,4,1)){
                  $awaypenalty = (int) substr($goalscored -> score,4,1);
                  $awaygoalIcon = "❌";
                 }
               else {
                  $awaypenalty = (int) substr($goalscored -> score,4,1);
                  $awaygoalIcon = "⚽";
                  }

                $homegoalScorer = $goalscored -> home_scorer;
               foreach($teamFlags as $key => $value)
                 {
              if($element -> match_hometeam_name == $key)
                  $homeflag = $value;
                }
                foreach($teamFlags as $key => $value)
                 {
              if($element -> match_awayteam_name == $key)
                  $awayflag = $value;
                }
               $awaygoalScorer = $goalscored -> away_scorer;
               
              $myanswer .= $homegoalIcon." ".$homeflag." ".$homegoalScorer."(120') \n".$awaygoalIcon." ".$awayflag." ".$awaygoalScorer."(120') \n ";
               
          }
          else
            {
              if($goalscored -> home_scorer)
             {
               $goalScorer = $goalscored -> home_scorer;
               foreach($teamFlags as $key => $value)
                 {
              if($element -> match_hometeam_name == $key)
                  $flag = $value;
                }
             }
             elseif ($goalscored -> away_scorer)
             {
               foreach($teamFlags as $key => $value)
                 {
              if($element -> match_awayteam_name == $key)
                  $flag = $value;
                }
               $goalScorer = $goalscored -> away_scorer;
               }

             $goalIcon = "⚽";
             $time =   $goalscored -> time;
             if($time == "&nbsp;")
             {
              $time = "120'";
              $goalIcon = "❌";
             }
              $myanswer .= $goalIcon." ".$flag." ".$goalScorer."(".$time.") \n ";
            }
         }
         foreach($element -> cards as $card)
         {
             if($card -> home_fault)
             {
               $cardHolder = $card -> home_fault;
               foreach($teamFlags as $key => $value)
                 {
              if($element -> match_hometeam_name == $key)
                  $flag = $value;
                }
             }
             elseif ($card -> away_fault)
             {
               foreach($teamFlags as $key => $value)
                 {
              if($element -> match_awayteam_name == $key)
                  $flag = $value;
                }
               $cardHolder = $card -> away_fault;
               }
            if($card -> card == 'yellowcard')
             {
               $cardcolor = "⚠️";
             }
            elseif($card -> card == 'redcard')
             {
               $cardcolor = "🔴";
             }
         $myanswer .= $cardcolor." ".$flag." ".$cardHolder."(".$card -> time.") \n ";
         }
         $myanswer .= "------------------------------ \n";
     }
}
if($myanswer == "")
 $myanswer = "Sorry , no World Cup matches are live currently. Please message *Next Match* to get details of the next match";

$finalanswer = $ongoing . $myanswer;

 $output["speech"] = '"'.$finalanswer.'"'; 
 $output["displayText"] = '"'.$finalanswer.'"'; 
 $output["source"] = "whatever.php";
 ob_end_clean(); 
 echo json_encode($output); 
?>