"use strict";
const express = require('express')
const app = express()
const port = 3000

const bodyParser = require("body-parser");
const axios = require('axios');
const uniqid = require('uniqid');
const fs = require('fs');
var resellerCall = require('./reseller');
const firebase = require('./firebase')
// Import Admin SDK
var admin = require("firebase-admin");
var pnaCall = require('./pna');
var rmaCall = require('./rmadetail');
var orderCall = require('./orderdetail');
var myAnswer;
var apiresponse = {};
/** bodyParser.urlencoded(options)
 * Parses the text as URL encoded data (which is how browsers tend to send form data from regular forms set to POST)
 * and exposes the resulting object (containing the keys and values) on req.body
 */
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(bodyParser.json());

app.post("/", function (req, res) {

var data = JSON.parse(req.body.data);


var v = Date('ymd');
const query = data.text;
const sessionID = data.from;
var output;
var dialogflowResult;


// Get a database reference to our blog
var db = admin.database();
var ref = db.ref("/");
var matches = data.text.match(/[A-Za-z].*[0-9]|[0-9].*[A-Za-z]/);
console.log(matches);
  axios({
  method: 'post',
  url: 'https://api.api.ai/v1/query?v='+v,
  data: { 'query' : [query], 'lang' :'en' , 'sessionId': sessionID},
  headers:{ 'Content-Type': 'application/json', 'Authorization': 'Bearer 2911c59a90ce4ff7ad4bedf90b37ebff' }
}).then(function (response) {
  myAnswer = response.data.result.fulfillment.speech;
  apiresponse.apiwha_autoreply = myAnswer;
  res.send(JSON.stringify(apiresponse));
  })
  .catch(function (error) {
   console.log(error);
  });


});


app.post("/webhook", function (req, res) {
switch (req.body.result.metadata.intentName) {
     case 'ResellerDetails': 
                    resellerCall(req.body);
                     break;
     case 'SKU_intent':
                    pnaCall(req.body,res);
                     break;
    case 'order_number':
                    orderCall(req.body,res);
                    break;
    case 'RMA':
                    rmaCall(req.body,res);
                    break;
    
     default:
         break;
 }


});

app.listen(port, () => console.log(`Example app listening on port ${port}!`))