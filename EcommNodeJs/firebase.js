var admin = require("firebase-admin");

var serviceAccount = require("./firebase-servicekey.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://whatsappagent-51790.firebaseio.com"
});