
const axios = require('axios');
const express = require('express')
const app = express()
var output = {};

const bodyParser = require("body-parser");

function orderCall(request,res) {
 var orderNumber = request.result.parameters.parameter.toString();
 if(orderNumber.indexOf('-') == -1 )
{
   orderNumber = orderNumber.substring(0,2) + '-' + orderNumber.substring(2);
}
    var resellerNo = '10-800573';
    var country = 'MX';
   var jsondata = {"servicerequest":
                      {"requestpreamble":
                          {"isocountrycode": country,
                           "customernumber": resellerNo
                          },
                          "ordersearchrequest":
                          {"systemid":"A300",
                          "searchcriteria":{"ordernumber":orderNumber},
                          "sortcriteria":[{"orderby":"ordernumber","orderbydirection":"DESC"}],
                          "pagenumber":"1"}
                        }
                      };
var url = "https://api.ingrammicro.com:443/vse/orders/v2/ordersearch";
var lineItems = "";

 axios({
  method: 'post',
  url: url,
  data: jsondata,
  headers:{ 'Accept': 'application/json',
    'Content-Type': 'application/json',
    'Authorization': 'Basic QVBQQ0hBVEJPVDpAMTZQYzdUMm90' }
}).then(function (response) {
  // dialogflowResult = JSON.parse(response);
if(response.data.serviceresponse.ordersearchresponse.numberoforderresults != '0')
{
    
var order = "Order# : "+ request.result.parameters.parameter + " - "+ response.data.serviceresponse.ordersearchresponse.ordersummary[0].orderstatus + "\nOrderDate :" + response.data.serviceresponse.ordersearchresponse.ordersummary[0].entrytimestamp + "\n\n";

if(response.data.serviceresponse.ordersearchresponse.ordersummary[0].lines)
{
 var totalLines =  response.data.serviceresponse.ordersearchresponse.ordersummary[0].lines.length;
 for (let line of response.data.serviceresponse.ordersearchresponse.ordersummary[0].lines) 
    {
     lineItems += "\n------------------------------ \n*" + line.partdescription1 +"* \nSKU : " + line.partnumber + " | VPN : " + line.manufacturerpartnumber + " | Quantity : " + line.requestedquantity;
    }

var finalAnswer = order + totalLines + " line item(s) in the order" + lineItems;
output.speech = finalAnswer; 
 output.displayText = finalAnswer;  
}
}
else{
var finalAnswer = "No orders found for current order, please enter another order number";
output.speech = finalAnswer; 
output.displayText = finalAnswer;  
}

  res.send(JSON.stringify(output));
  })
  .catch(function (error) {
   console.log(error);
  });;
};

module.exports =   orderCall ;