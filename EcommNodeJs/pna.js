
const axios = require('axios');
const express = require('express')
const app = express()
var output = {};
var ogResellerNo,country;

const bodyParser = require("body-parser");

function pnaCall(request,res) {


var resellerNo = request.result.contexts[0].parameters.resellerNo.toString();

    var skuNumber = request.result.contexts[0].parameters.parameter.toUpperCase();
    if(resellerNo.indexOf('-') == -1 )
{
   resellerNo = resellerNo.substring(0,2) + '-' + resellerNo.substring(2);
}
//     var resellerNo = '10000001';
    var country = 'MX';
   var jsondata = {  
   "servicerequest":{  
      "priceandstockrequest":{  
         "showwarehouseavailability":"True",
         "extravailabilityflag":"Y",
         "item":[  
            {  
               "ingrampartnumber":  skuNumber,
               "quantity":1
            }
         ],
         "includeallsystems":false
      },
      "requestpreamble":{  
         "customernumber": resellerNo, 
         "isocountrycode": country
      }
   }
};
var url = "https://api.ingrammicro.com:443/multiskupriceandstockapi_v4";
var totalPrice = 0;
var totalQty = 0 ;
 axios({
  method: 'post',
  url: url,
  data: jsondata,
  headers:{ 'Accept': 'application/json',
    'Content-Type': 'application/json',
    'Authorization': 'Basic QVBQQ0hBVEJPVDpAMTZQYzdUMm90' }
}).then(function (response) {
  // dialogflowResult = JSON.parse(response);
var price = response.data.serviceresponse.priceandstockresponse.details[0].currency + " " + response.data.serviceresponse.priceandstockresponse.details[0].customerprice;

if(response.data.serviceresponse.priceandstockresponse.details[0].partdescription1)
{
    var description = response.data.serviceresponse.priceandstockresponse.details[0].partdescription1;
}
else{
var description = response.data.serviceresponse.priceandstockresponse.details[0].partdescription2;
}
var vpn = response.data.serviceresponse.priceandstockresponse.details[0].vendorpartnumber;


for (let item of response.data.serviceresponse.priceandstockresponse.details[0].warehousedetails) {
     var qty = parseInt(item.availablequantity);
        totalQty = totalQty + qty; 
}

// response.data.serviceresponse.priceandstockresponse.details[0].warehousedetails.foreach(function(element){
//     var qty = parseInt(element.availablequantity);
//         totalQty = totalQty + qty;
// })


if(response.data.serviceresponse.responsepreamble.responsestatus == "SUCCESS")
{

 var finalAnswer = "*" + description + "*\nVPN : " + vpn + " | SKU : " + request.result.parameters.parameter + "\n_" + price + " | " + totalQty + " In Stock_";
 output.speech = finalAnswer; 
 output.displayText = finalAnswer;
}
else
{
 output.speech = response.data.serviceresponse.responsepreamble.responsemessage; 
 output.displayText = response.data.serviceresponse.responsepreamble.responsemessage;
} 
  res.send(JSON.stringify(output));
  })
  .catch(function (error) {
   console.log(error);
  });
};

module.exports =   pnaCall ;