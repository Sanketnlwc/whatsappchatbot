<?php 

// This is your webhook. You must configure it in the number settings section.


$sessionID = uniqid('',true);



// $data1 = '{ 
//  "id": "7797", 
//  "number": "[YOUR NUMBER]", 
//  "from": "918286955395", 
//  "to": "[TO NUMBER]", 
//  "type": "IN", 
//  "text": "hi",	
//  "creation_date": "2017-03-18 14:49:23" 
// }';

$data = json_decode($_POST["data"]); 

// $data = json_decode($data1); 

include('firebaseconnection.php');

$text = $data->text; 

if (preg_match('/[A-Za-z].*[0-9]|[0-9].*[A-Za-z]/', $data->text)) 
      $text = $data->text." ".$data->from;

if(!($database -> getReference()->getSnapshot()->hasChild($data->from)))
{
    $postData = [
    'ResellerNo' => '',
    'Country' => 'MX',
];


$updates = [
    $data->from => $postData
];
    $database -> getReference()->update($updates);
}

try {
    // if(isset($_POST['submit'])){
        $query = $text;
        $postData = array('query' => array($query), 'lang' => 'en', 'sessionId' => $sessionID);
        $jsonData = json_encode($postData);
        $v = date('Ymd');
        $ch = curl_init('https://api.api.ai/v1/query?v='.$v);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        // curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: Bearer a431286283604826af800cb786f5bb28'));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: Bearer 2911c59a90ce4ff7ad4bedf90b37ebff'));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $dialogflowresult = json_decode($result,true);
        curl_close($ch);
    // }
}
catch (Exception $e) {
    $speech = $e->getMessage();
    $fulfillment = new stdClass();
    $fulfillment->speech = $speech;
    $result = new stdClass();
    $result->fulfillment = $fulfillment;
    $response = new stdClass();
    $response->result = $result;
    echo json_encode($response);
}



// When you receive a message an INBOX event is created 
if ($data->event=="INBOX") 
{ 
  $my_answer = $dialogflowresult['result']['fulfillment']['speech'];
  if(substr($my_answer, 0 , 1) == '"')
  {
   $my_answer = substr($my_answer,1,-1);
  }
   


  $response = new StdClass(); 
  $response->apiwha_autoreply = $my_answer; // Attribute "apiwha_autoreply" is very important! 

  echo json_encode($response); // Don't forget to reply in JSON format 


  /* You don't need any APIKEY to answer to your customer from a webhook */ 

} 

?>