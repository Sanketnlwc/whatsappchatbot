<?php 
$curl = curl_init();
$headers = get_headers($url);
switch ($method)
{
case "POST": curl_setopt($curl,CURLOPT_POST, 1);
             if ($jsondata)
             curl_setopt($curl,CURLOPT_POSTFIELDS, $jsondata);
             break;
default:
            if ($jsondata)
                $url = sprintf("%s?%s", $url,http_build_query($jsondata));
}
// Authentication
curl_setopt($curl,
CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
curl_setopt( $curl,CURLOPT_HTTPHEADER, array('Content-Type:application/json','Authorization:Basic QVBQQ0hBVEJPVDpAMTZQYzdUMm90'));
curl_setopt($curl, CURLOPT_URL,$url);
curl_setopt($curl,CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
$result = curl_exec($curl);
if (curl_error($curl)) {
    $error_msg = curl_error($curl);
}
curl_close($curl);

$response = json_decode($result,true);
?>