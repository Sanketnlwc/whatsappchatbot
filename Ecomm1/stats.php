<?php

date_default_timezone_set("Asia/Kolkata");
$to = '2018-06-14';
$from = '2018-07-15';
$countryID = 340;
$teams = '{
    "Russia":"🇷🇺",
    "Saudi Arabia":"🇸🇦",
    "Spain":"🇪🇸",
    "Portugal":"🇵🇹",
    "England":"🏴󠁧󠁢󠁥󠁮󠁧󠁿",
    "Egypt":"🇪🇬",
    "Senegal":"🇸🇳",
    "Germany":"🇩🇪",
    "Croatia":"🇭🇷",
    "Argentina":"🇦🇷",
    "Brazil":"🇧🇷",
    "Uruguay":"🇺🇾",
    "France":"🇫🇷",
    "Colombia":"🇨🇴",
    "Iran":"🇮🇷",
    "South Korea":"🇰🇷",
    "Australia":"🇦🇺",
    "Nigeria":"🇳🇬",
    "Poland":"🇵🇱",
    "Iceland":"🇮🇸",
    "Serbia":"🇷🇸",
    "Panama":"🇵🇦",
    "Swistzerland":"🇨🇭",
    "Morocco":"🇲🇦",
    "Tunisia":"🇹🇳",
    "Denmark":"🇩🇰",
    "Costa Rica":"🇨🇷",
    "Japan":"🇯🇵",
    "Mexico":"🇲🇽",
    "Peru":"🇵🇪",
    "Sweden":"🇸🇪",
    "Belgium":"🇧🇪"
}';

$teamFlags = json_decode($teams,true);
// if($time > "00:00:00" && $time < "03:00:00")
//   {
//   $from = date('Y-m-d',(strtotime ( '-1 day' , strtotime ( $from) ) ));
//   $to = date('Y-m-d',(strtotime ( '-1 day' , strtotime ( $to) ) ));
//   }
$team1 = $request["result"]["parameters"]["team1"];
$team2 = $request["result"]["parameters"]["team2"];
$curl_options = array(
  CURLOPT_URL => "https://apifootball.com/api/?action=get_events&from=2018-06-14&to=2018-07-15&country_id=340&APIkey=$APIkey",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_HEADER => false,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_CONNECTTIMEOUT => 0
);

$curl = curl_init();
curl_setopt_array( $curl, $curl_options );
$result1 = curl_exec( $curl );
$ongoing = " Statistics *".$team1."* vs *".$team2."* \n ------------------------------ \n";
$myanswer = "";


$result = (array) json_decode($result1);
foreach( $result as $element){
    if(($element -> match_hometeam_name == $team1 ||$element -> match_awayteam_name == $team1) && ($element -> match_hometeam_name == $team2 ||$element -> match_awayteam_name == $team2))
     {
         $myanswer .= "*".$element ->match_hometeam_name."* (".$element->match_hometeam_score.")  vs *".$element -> match_awayteam_name."* (".$element->match_awayteam_score.") (".$element -> match_status.") \n  ";
         
         foreach($element -> statistics as $stats)
         {
               foreach($teamFlags as $key => $value)
                 {
              if($element -> match_hometeam_name == $key)
                  $homeFlag = $value;
              if($element -> match_awayteam_name == $key)
                  $awayFlag = $value;
                }
         $myanswer .= "*".$stats -> type."* : ".$homeFlag." ".$stats -> home." | ".$awayFlag." ".$stats -> away." \n";
         }
     }
}
$finalanswer = $ongoing . $myanswer;

 $output["speech"] = '"'.$finalanswer.'"'; 
 $output["displayText"] = '"'.$finalanswer.'"'; 
 $output["source"] = "whatever.php";
 ob_end_clean(); 
 echo json_encode($output); 
?>