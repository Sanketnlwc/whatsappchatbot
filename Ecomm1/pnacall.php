<?php 

$ogResellerNo = $database->getReference($request["result"]["parameters"]["number"].'/ResellerNo')->getValue();
$country = $database->getReference($request["result"]["parameters"]["number"].'/Country')->getValue();

        if(strpos($ogResellerNo, '-') == false)
                        $resellerNo = substr($ogResellerNo,0,2)."-".substr($ogResellerNo,2);
                    else 
                        $resellerNo = $ogResellerNo;
                                       
$jsondata = '{  
   "servicerequest":{  
      "priceandstockrequest":{  
         "showwarehouseavailability":"True",
         "extravailabilityflag":"Y",
         "item":[  
            {  
               "ingrampartnumber":"'.strtoupper($request["result"]["parameters"]["parameter"]).'",
               "quantity":1
            }
         ],
         "includeallsystems":false
      },
      "requestpreamble":{  
         "customernumber":"'.$resellerNo.'",
         "isocountrycode":"'.$country.'"
      }
   }
}';
$url = "https://api.ingrammicro.com:443/multiskupriceandstockapi_v4";
$method = 'POST';
$totalPrice = 0;
// include('header.php');
$curl = curl_init();
$headers = get_headers($url);
switch ($method)
{
case "POST": curl_setopt($curl,CURLOPT_POST, 1);
             if ($jsondata)
             curl_setopt($curl,CURLOPT_POSTFIELDS, $jsondata);
             break;
default:
            if ($jsondata)
                $url = sprintf("%s?%s", $url,http_build_query($jsondata));
}
// Authentication
curl_setopt($curl,
CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
curl_setopt( $curl,CURLOPT_HTTPHEADER, array('Content-Type:application/json','Authorization:Basic QVBQQ0hBVEJPVDpAMTZQYzdUMm90'));
curl_setopt($curl, CURLOPT_URL,$url);
curl_setopt($curl,CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
$result = curl_exec($curl);
if (curl_error($curl)) {
    $error_msg = curl_error($curl);
}
curl_close($curl);

$response = json_decode($result,true);
$totalQty = (int)0;

$price = $response['serviceresponse']['priceandstockresponse']['details'][0]['currency']." ".$response['serviceresponse']['priceandstockresponse']['details'][0]['customerprice'];

if($response['serviceresponse']['priceandstockresponse']['details'][0]['partdescription1'])
{
    $description = $response['serviceresponse']['priceandstockresponse']['details'][0]['partdescription1'];
}
else{
$description = $response['serviceresponse']['priceandstockresponse']['details'][0]['partdescription2'];
}
$vpn = $response['serviceresponse']['priceandstockresponse']['details'][0]['vendorpartnumber'];




foreach ($response['serviceresponse']['priceandstockresponse']['details'][0]['warehousedetails'] as $element) {
    $qty = (int)$element['availablequantity'];
 $totalQty = $totalQty + $qty;
}

if($response['serviceresponse']['responsepreamble']['responsestatus']== "SUCCESS")
{

 $finalAnswer = "*".$description."*\nVPN : ".$vpn." | SKU : ".$request["result"]["parameters"]["parameter"]."\n_".$price." | ".$totalQty." In Stock_";
 $output["speech"] = $finalAnswer; 
 $output["displayText"] = $finalAnswer; 
 $output["source"] = "whatever.php";
}
else
{
 $output["speech"] = $response['serviceresponse']['responsepreamble']['responsemessage']; 
 $output["displayText"] = $response['serviceresponse']['responsepreamble']['responsemessage']; 
 $output["source"] = "whatever.php";
} 

 ob_end_clean(); 
 echo json_encode($output); 
//  print_r($totalQty);
?>