<?php 


$orderNumber = $request["result"]["parameters"]["parameter"];

$jsondata = '{"servicerequest":
                      {"requestpreamble":
                          {"isocountrycode":"MX",
                           "customernumber":"10-900090"
                          },
                          "ordersearchrequest":
                          {"systemid":"A300",
                          "searchcriteria":{"ordernumber":"'.$orderNumber.'"},
                          "sortcriteria"
                          :[{"orderby":"ordernumber","orderbydirection":"DESC"}],
                          "pagenumber":"1"}
                        }
                      }';
$url = 'https://api.ingrammicro.com:443/vse/orders/v2/ordersearch';


$method = "POST";

$curl = curl_init();
$headers = get_headers($url);
switch ($method)
{
case "POST": curl_setopt($curl,CURLOPT_POST, 1);
             if ($jsondata)
             curl_setopt($curl,CURLOPT_POSTFIELDS, $jsondata);
             break;
default:
            if ($jsondata)
                $url = sprintf("%s?%s", $url,http_build_query($jsondata));
}
// Authentication
curl_setopt($curl,
CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
curl_setopt( $curl,CURLOPT_HTTPHEADER, array('Content-Type:application/json','Authorization:Basic QVBQQ0hBVEJPVDpAMTZQYzdUMm90'));
curl_setopt($curl, CURLOPT_URL,$url);
curl_setopt($curl,CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
$result = curl_exec($curl);
if (curl_error($curl)) {
    $error_msg = curl_error($curl);
}
curl_close($curl);
$lineItems="";
$response = json_decode($result,true);


if($response['serviceresponse']['ordersearchresponse']['numberoforderresults'] != '0')
{
    
$order = "Order# : ".$request["result"]["parameters"]["parameter"]."  - ".$response['serviceresponse']['ordersearchresponse']['ordersummary'][0]['orderstatus']."\nOrderDate :".$response['serviceresponse']['ordersearchresponse']['ordersummary'][0]['entrytimestamp']."\n\n";

if($response['serviceresponse']['ordersearchresponse']['ordersummary'][0]['lines'])
{
 $totalLines =  sizeof($response['serviceresponse']['ordersearchresponse']['ordersummary'][0]['lines']);
 foreach($response['serviceresponse']['ordersearchresponse']['ordersummary'][0]['lines'] as $line)
    {
     $lineItems .= "\n------------------------------ \n*".$line['partdescription1']."* \nSKU : ".$line['partnumber']." | VPN : ".$line['manufacturerpartnumber']." | Quantity : ".$line['requestedquantity'];
    }
}

$finalAnswer = $order.$totalLines." line item(s) in the order".$lineItems;
$output["speech"] = $finalAnswer; 
 $output["displayText"] = $finalAnswer;  
 $output["source"] = "whatever.php"; 
}
else{
$finalAnswer = "No orders found for current order, please enter another order number";
$output["speech"] = $finalAnswer; 
 $output["displayText"] = $finalAnswer;  
 $output["source"] = "whatever.php"; 
}

 ob_end_clean(); 
 echo json_encode($output); 
?>