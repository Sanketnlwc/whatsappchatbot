<?php 

// This is your webhook. You must configure it in the number settings section.

$data = json_decode($_POST["data"]); 

$sessionID = uniqid('',true);

try {
    // create curl resource
    $ch = curl_init();
    // set url
    curl_setopt($ch, CURLOPT_URL, "https://api.api.ai/v1/query?v=20150910&e=WELCOME&lang=en&sessionId=" . $sessionID);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: Bearer 46a36bd310364edcb7c1893ffad8f03f'));
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    //return the transfer as a string
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    // $output contains the output string
    $output = curl_exec($ch);
    // close curl resource to free up system resources
    curl_close($ch);
}catch (Exception $e) {
    $speech = $e->getMessage();
    $fulfillment = new stdClass();
    $fulfillment->speech = $speech;
    $result = new stdClass();
    $result->fulfillment = $fulfillment;
    $response = new stdClass();
    $response->result = $result;
    echo json_encode($response);
}

try {
    // if(isset($_POST['submit'])){
        $query = $data->text;
        $postData = array('query' => array($query), 'lang' => 'en', 'sessionId' => $sessionID);
        $jsonData = json_encode($postData);
        $v = date('Ymd');
        $ch = curl_init('https://api.api.ai/v1/query?v='.$v);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: Bearer 46a36bd310364edcb7c1893ffad8f03f'));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $dialogflowresult = json_decode($result,true);
        curl_close($ch);
    // }
}
catch (Exception $e) {
    $speech = $e->getMessage();
    $fulfillment = new stdClass();
    $fulfillment->speech = $speech;
    $result = new stdClass();
    $result->fulfillment = $fulfillment;
    $response = new stdClass();
    $response->result = $result;
    echo json_encode($response);
}



// When you receive a message an INBOX event is created 
if ($data->event=="INBOX") 
{ 
  $my_answer = $dialogflowresult['result']['fulfillment']['speech'];
  if(substr($my_answer, 0 , 1) == '"')
  {
   $my_answer = substr($my_answer,1,-1);
  }
   


  $response = new StdClass(); 
  $response->apiwha_autoreply = $my_answer; // Attribute "apiwha_autoreply" is very important! 

  echo json_encode($response); // Don't forget to reply in JSON format 

  /* You don't need any APIKEY to answer to your customer from a webhook */ 

} 

?>